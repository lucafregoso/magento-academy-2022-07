<?php
namespace Hrm\Fidelity\Setup;

use \Magento\Eav\Setup\EavSetup;
use \Magento\Eav\Setup\EavSetupFactory;
use \Magento\Framework\Setup\InstallDataInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use \Magento\Eav\Model\Config;
use \Magento\Customer\Model\Customer;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

	 /**
     * @var CustomerSetup
     */
    private $customerSetupFactory;

	/**
     * @var SetFactory
     */
    private $attributeSetFactory;

	public function __construct(
		EavSetupFactory $eavSetupFactory, 
		Config $eavConfig, 
		CustomerSetupFactory $customerSetupFactory,
		SetFactory $attributeSetFactory
		)
	{
		$this->eavSetupFactory 			= $eavSetupFactory;
		$this->eavConfig       			= $eavConfig;
		$this->customerSetupFactory     = $customerSetupFactory;
        $this->attributeSetFactory 		= $attributeSetFactory;
	}

	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

		$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

		$eavSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			'fidelity_subscription',
			[
				'type'         => 'int',
				'label'        => 'Fidelity Subscription',
				'input'        => 'boolean',
				'required'     => false,
                'default'      => 0,
                'sort_order'   => 900,
                'visible'      => true,
				'user_defined' => true,
				'system'       => 0,
				'source' 	   => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean'


			]
		);
		$fidelitySubscription = $this->eavConfig->getAttribute(Customer::ENTITY, 'fidelity_subscription');

		$fidelitySubscription->setData(
			'used_in_forms', ['adminhtml_customer','customer_account_create']
		);

        $fidelitySubscription->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId
        ]);

		$fidelitySubscription->save();
	}
}