<?php declare(strict_types=1);

namespace Emanuele\Framework\View\Asset\PreProcessor;

use Magento\Framework\View\Asset\PreProcessor\AlternativeSource;
use Magento\Framework\View\Asset\PreProcessor\AlternativeSourceInterface;

class FileNameResolver extends \Magento\Framework\View\Asset\PreProcessor\FileNameResolver
{
    /**
     * @var AlternativeSource[]
     */
    private $alternativeSources;

    public function __construct(array $alternativeSources = [])
    {
        parent::__construct($alternativeSources);

        $this->alternativeSources = array_map(
            function (AlternativeSourceInterface $alternativeSource) {
                return $alternativeSource;
            },
            $alternativeSources
        );
    }

    /**
     * Resolve filename
     *
     * @param string $fileName
     * @return string
     */
    public function resolve($fileName)
    {
        $compiledFile = $fileName;

        /**
         * I do this because of:
         * Error happened during deploy process: Deprecated Functionality: pathinfo():
         * Passing null to parameter #1 ($path) of type string is deprecated
         * in /var/www/html/vendor/magento/framework/View/Asset/PreProcessor/FileNameResolver.php on line 44
         */
        $fileName = $fileName ?? '';

        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        foreach ($this->alternativeSources as $name => $alternative) {
            if ($alternative->isExtensionSupported($extension)
                && strpos(basename($fileName), '_') !== 0
            ) {
                $compiledFile = substr($fileName, 0, strlen($fileName) - strlen($extension) - 1);
                $compiledFile .= '.' . $name;
            }
        }
        return $compiledFile;
    }
}
