<?php
 
/**
 * @category  Webkul
 * @package   Webkul_CronSetup
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 
namespace Webkul\CronSetup\Cron;
 
/**
 * custom cron actions
 */
class CronFile
{
    public function execute()
    {
        // do your custom code as your requirement
    }
}
