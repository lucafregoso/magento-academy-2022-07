<?php

/**
 * @category Magento_Command
 * @package  HRM/PagesImporter
 * @author   HRM - Luca Fregoso <luca.fregoso@hrmgroup.it>
 * @license  https://opensource.org/licenses/mit MIT License
 * @link     https://hrm.group
 */

namespace HRM\PagesImporter\Commands;

use DOMDocument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\Glob;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\State;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use HRM\PagesImporter\Model\ReadDATA;
use HRM\PagesImporter\Model\CreateBlockHtml;
use HRM\PagesImporter\Model\CreateBlockTxt;
/**
 * Class PagesImporterCommand
 *
 * Import pages from txt and html files in a specific folder.
 * The filename becomes the page title, the file content becomes the page content.
 */
class PagesImporterCommand extends Command {

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(State $state, File $file, Filesystem $filesystem, ReadDATA $readdata, CreateBlockHtml $createblockhtml, CreateBlockTxt $createblocktxt) {
        $state->setAreaCode('adminhtml');

        $this->_state = $state;
        $this->_file = $file;
        $this->_filesystem = $filesystem;
        $this->_readdata = $readdata;
        $this->_createblockhtml = $createblockhtml;
        $this->_createblocktxt = $createblocktxt;
        parent::__construct();
    }

    /**
     * Configure command
     *
     * @return void
     */
    protected function configure() {
        $this->setName('hrm:pages:import');
        $this->setDescription("Import pages reading txt and html files in a specific folder.");
        $this->addArgument(
                'folder-path', InputArgument::REQUIRED, 'Path to the import folder (relative to Magento bin directory).'
        );

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        if (!$this->_state->getAreaCode()) {
            $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        }
        try {
            $folderPath = $input->getArgument('folder-path');
            $fileNames = $this->_readdata->sayData($folderPath, $output);

            foreach ($fileNames as $fileName) {
                $fileInfo = $this->_file->getPathInfo($fileName);
                $pageTitle = str_replace(
                        ['-', '_', '.', $fileInfo['basename']], [' ', ' ', ' ', ''], $fileInfo['basename']
                );

                switch ($fileInfo['extension']) {
                    case 'html':
                        $output->writeln('<info>Loading HTML</info>');
                        libxml_use_internal_errors(true);
                        $domDocument = new DOMDocument();
                        $file = $domDocument->loadHTML(file_get_contents($fileName));
                        $bodyElement = $domDocument->getElementsByTagName('body')[0];
                        $pageContent = $domDocument->savehtml($bodyElement);
                        $html = file($fileName); //file in to an array
                        $html_title = explode('Block Title:', $html[0]);
                        $html_content = explode('Block Content:', $html[2]);
                        $title = trim($html_title[1]); //space 1
                        $content =  trim($html_content[1]); //space 3
                        $identifier = str_replace(" ", "_", strtolower($title . "_" . rand(0,100))); //space 1 
                        $is_active = (string) 1;
                        $output->writeln('<comment>Loading Title ' . $title . '</comment>');
                        $output->writeln('<comment>Loading Content ' . $content . '</comment>');
                        $output->writeln('<comment>Loading Identifier ' . $identifier . '</comment>');
                        $output->writeln('<comment>Loading Active ' . $is_active . '</comment>');
                        $blockHtmlNames = $this->_createblockhtml->sayBlockHtml($title, $identifier, $content, $is_active, $output);
                        break;
                    case 'txt':
                        $output->writeln('<info>Loading TXT</info>');
                        $pageContent = file_get_contents($fileName);
                        $txt = file($fileName); //file in to an array
                        $txt_title = explode('Block Title:', $txt[0]);
                        $txt_content = explode('Block Content:', $txt[2]);
                        $title = trim($txt_title[1]); //space 1
                        $content =  trim($txt_content[1]); //space 3
                        $identifier = str_replace(" ", "_", strtolower($title . "_" . rand(0,100))); //space 1 
                        $is_active = (string) 1;
                        $output->writeln('<comment>Loading Title ' . $title . '</comment>');
                        $output->writeln('<comment>Loading Content ' . $content . '</comment>');
                        $output->writeln('<comment>Loading Identifier ' . $identifier . '</comment>');
                        $output->writeln('<comment>Loading Active ' . $is_active . '</comment>');
                        $blockTxtNames = $this->_createblocktxt->sayBlockTxtl($title, $identifier, $content, $is_active, $output);
                        break;
                }
            }
        } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
        }
    }

}