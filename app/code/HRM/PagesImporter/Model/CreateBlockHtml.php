<?php

/**
 * @category Magento_Command
 * @package  HRM/PagesImporter
 * @author   HRM - Luca Fregoso <luca.fregoso@hrmgroup.it>
 * @license  https://opensource.org/licenses/mit MIT License
 * @link     https://hrm.group
 */

namespace HRM\PagesImporter\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\Glob;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Cms\Model\BlockFactory;

class CreateBlockHtml extends AbstractModel {

    public function __construct(BlockFactory $blockFactory) {
        $this->_blockFactory = $blockFactory;
    }

    public function sayBlockHtml($title, $identifier, $content, $is_active, $output) {
        // Build configuration
        $cmsBlockData = [
            'title' => $title,
            'identifier' => $identifier,
            'content' => $content,
            'is_active' => $is_active,
            'stores' => [0],
            'sort_order' => 0
        ];
        $this->_blockFactory->create()->setData($cmsBlockData)->save();
        $output->writeln('<info>New record created successfully</info>');
    }

}
