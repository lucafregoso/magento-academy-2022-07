<?php

/**
 * @category Magento_Command
 * @package  HRM/PagesImporter
 * @author   HRM - Luca Fregoso <luca.fregoso@hrmgroup.it>
 * @license  https://opensource.org/licenses/mit MIT License
 * @link     https://hrm.group
 */

namespace HRM\PagesImporter\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\Glob;
use Magento\Framework\Filesystem\Io\File;

class ReadDATA extends AbstractModel 
{

    public function sayData($folderPath, $output) {
        $output->writeln('<info>Loading Pages from ' . $folderPath . '</info>');
        $fileNames = glob("{$folderPath}/*.{html,txt}", GLOB_BRACE);

        return $fileNames;
    }

}
