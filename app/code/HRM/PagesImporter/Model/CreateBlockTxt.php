<?php

/**
 * @category Magento_Command
 * @package  HRM/PagesImporter
 * @author   HRM - Luca Fregoso <luca.fregoso@hrmgroup.it>
 * @license  https://opensource.org/licenses/mit MIT License
 * @link     https://hrm.group
 */

namespace HRM\PagesImporter\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Filesystem\Glob;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class CreateBlockTxt extends AbstractModel {

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(Filesystem $filesystem) {
        $this->_filesystem = $filesystem;
    }

    public function sayBlockTxtl($title, $identifier, $content, $is_active, $output) {
        // Build configuration
        $etcDir = $this->_filesystem->getDirectoryRead(DirectoryList::CONFIG)->getAbsolutePath();
        // $etcDir = "/srv/www/vhosts/pasticcinformatici/magento2.pasticcinformatici.com/website/app/etc/";
        $envFile = $etcDir . 'env.php';
        $envData = (include $envFile);

        $database = $envData['db']['connection']['default']['dbname'];
        $servername = $envData['db']['connection']['default']['host'];
        $username = $envData['db']['connection']['default']['username'];
        $password = $envData['db']['connection']['default']['password'];

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $database);
        // Check connection
        if (!$conn) {
            $output->writeln('<error>Connection failed: ' . mysqli_connect_error() . '</error>');
            mysqli_close($conn);
        }

        $output->writeln('<info>Connected successfully</info>');

        $sqlUno = "INSERT INTO cms_block (title, identifier, content, is_active) VALUES ('$title', '$identifier', '$content', '$is_active')";
        // $sql = 'INSERT INTO cms_block (title, identifier, content, is_active) VALUES (\'$title\', \'$identifier\', \'$content\', \'$is_active\')';
        if (mysqli_query($conn, $sqlUno)) {
            $output->writeln('<info>New record created successfully</info>');
        } else {
            $output->writeln('<error>Error: ' . $sqlUno . '\n' . mysqli_error($conn) . '</error>');
        }

        $sqlDue = "SELECT block_id FROM cms_block WHERE identifier = '$identifier'";
        $result = mysqli_query($conn, $sqlDue);
   
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                // echo "id: " . $row["block_id"] . " - title: " . $row["title"] . " " . $row["is_active"] . "<br>";
                $output->writeln('<comment>Loading Id ' . $row["block_id"] . '</comment>');
                $blockId = $row["block_id"];
            }
        } else {
            $output->writeln('<info>0 results</info>');
        }
        
        $sqlTre = "INSERT INTO cms_block_store (block_id, store_id) VALUES ('$blockId', '0')";
        if (mysqli_query($conn, $sqlTre)) {
            $output->writeln('<info>New record created successfully</info>');
        } else {
            $output->writeln('<error>Error: ' . $sqlTre . '\n' . mysqli_error($conn) . '</error>');
        }
 
        mysqli_close($conn);
    }

}