<?php

namespace HRM\ZhifanImportCSVCommand\Console\Command;

use HRM\ZhifanImportCSVCommand\Model\AttributeCreator;
use HRM\ZhifanImportCSVCommand\Model\CategoryCreator;
use HRM\ZhifanImportCSVCommand\Model\ProductCreator;
use HRM\ZhifanImportCSVCommand\PHPandas\DataFrame\DataFrame;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCSVCommand extends Command
{
    private $_categoryLinkManagement;
    private $_categoryCollectionFactory;
    // private $_storeFactory;
    // private StoreManagerInterface $_storeManager;
    private StoreManagerInterface $_storeManager;
    private CategoryRepository $_categoryRepository;
    private ProductCreator $_productCreator;
    private CategoryCreator $_categoryCreator;
    private AttributeCreator $_attributeCreator;

    const PATH_CSV = "path_csv";

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        StoreManagerInterface $storeManager,
        CategoryCreator $categoryCreator,
        \Magento\Framework\App\State $state,
        // ObjectManager $objectManager,
        // ObjectManager $objectManager,
        // \Magento\Framework\ObjectManager\ObjectManager $objectManager,
        ProductCreator $productCreator,
        AttributeCreator $attributeCreator,
    ) {
        // $objectManager->configure([
        //     'preferences' => [
        //         'Magento\Store\Model\StoreManagerInterface' => 'Magento\Store\Model\StoreManager',
        //     ]
        // ]);

        // We cannot use core functions (like saving a category) unless the area
        // code is explicitly set.
        try {
            // if (!$state->getAreaCode()) {
            $state->setAreaCode('adminhtml');
            // }
        } catch (\Exception $e) {
            // Intentionally left empty.
            // to check for error uncomment the following line
            // var_dump($e->getMessage());
        }

        $this->_attributeCreator = $attributeCreator;
        $this->_productFactory = $productFactory;
        $this->_categoryLinkManagement = $categoryLinkManagement;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        // $this->_storeFactory = $storeFactory;

        // $this->_storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $this->_storeManager = $storeManager;
        // $this->_categoryRepository = $objectManager->get(CategoryRepository::class);

        $this->_categoryCreator = $categoryCreator;
        $this->_productCreator = $productCreator;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        /**
         * SKU (CODICE ARTICOLO);DESCRIZIONE (NOME ARTICOLO);FAMIGLIA;SOTTO-FAMIGLIA;GRUPPO;SOTTOGRUPPO;;;;DIMENSIONI;MATERIALE;COLORE;PESO NETTO;PESO LORDO;VOLUME;GIACENZA STONES;IMPEGNATO STONES;GIACENZA VOL;IMPEGNATO VOL;
         */
        $test = [
            'Default Category' => [
                'Test' => [
                    'Prova' => [
                        'Sottoprova' => []
                    ],
                    'Coso' => [
                        'Roso' => []
                    ],
                    'Cacca' => [
                        'Rosa' => []
                    ]
                ]
            ]
        ];
        //
        // $this->_categoryCreator->createHierarchy($test);
        $pathCsv = $input->getArgument(self::PATH_CSV);
        $output->writeln("Importing '$pathCsv'");

        $dataframe = DataFrame::loadFromCSV($pathCsv);

        // foreach ($categories as $category) {
        //     $productCategories[$category->getName()] = $category->getId();
        //     dump($category->getName());
        //
        //     // $productCategories[$category->getName()] = [
        //     //     'store_id' => $this->_storeManager->getStore()->getId(),
        //     //     'category' => $category->getId(),
        //     // ];
        // }

        // $this->_categoryCreator->create();
        // die();
        define('CSV_CATEGORY_FIELDS', ['FAMIGLIA', 'SOTTO-FAMIGLIA', 'GRUPPO', 'SOTTOGRUPPO']);
        foreach ($dataframe as $row) {
            $categoryHierarchy = [
                'Default Category' => []
            ];

            $productSku = $row->getByColumnName('SKU (CODICE ARTICOLO)');

            if (is_null($productSku)) {
                continue;
            }

            $productName = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productDescription = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productShortDescription = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productPrice = $row->getByColumnName('PREZZO', 0.0);
            $productWeight = $row->getByColumnName('PESO NETTO', 0.0);

            $output->writeln("<info> Processing name = $productName, SKU = $productSku, price = $productPrice. </>");

            $parentCategoryName = 'Default Category';
            $current = &$categoryHierarchy;
            foreach (CSV_CATEGORY_FIELDS as $csvCategoryField) {
                $csvCategoryValue = $row->getByColumnName($csvCategoryField);

                if (is_null($csvCategoryValue) || empty($csvCategoryValue)) {
                    break;
                }

                $current[$parentCategoryName] = [
                    "$csvCategoryValue" => []
                ];

                $current = &$current[$parentCategoryName];

                $parentCategoryName = $csvCategoryValue;
            }

            $productCategoryIds = $this->_categoryCreator->createHierarchy($categoryHierarchy);

            /**
             *
             * Create Attribute option
             *
             */

            $attributeValue = $row->getByColumnName('COLORE', '');

            if ($attributeValue != '') {
                print("Attribute Value: {$attributeValue}\n");
                $this->_attributeCreator->addValueToAttribute(
                    name: 'color',
                    value: $attributeValue
                );
                sleep(1);
            }

            $newProduct = $this->_productCreator->getOrCreate(
                sku: $productSku,
                name: $productName,
                description: $productDescription,
                shortDescription: $productShortDescription,
                price: $productPrice,
                weight: $productWeight,
                status: Status::STATUS_ENABLED,
                visibility: Visibility::VISIBILITY_BOTH
            );

            $this->_categoryLinkManagement->assignProductToCategories(
                $newProduct->getSku(),
                $productCategoryIds
            );

            // dump($productCategoryIds);
        }

        $output->writeln([
            "<info> Import completed </>",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("hrm:zhifanimportcsvcommand:importcsv");
        $this->setDescription("Command to import CSV");
        $this->setDefinition([
            new InputArgument(self::PATH_CSV, InputArgument::REQUIRED, "Path to CSV"),
        ]);
        parent::configure();
    }

    private function slugify($string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }
}
