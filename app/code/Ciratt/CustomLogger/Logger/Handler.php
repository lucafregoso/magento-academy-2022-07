<?php
namespace Ciratt\CustomLogger\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $fileName = '/var/log/custom_logger.log';
    protected $loggerType = \Monolog\Logger::INFO;
}