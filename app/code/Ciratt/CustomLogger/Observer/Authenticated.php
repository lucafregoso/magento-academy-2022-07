<?php
namespace Ciratt\CustomLogger\Observer;

use Magento\Framework\Event\ObserverInterface;

class Authenticated implements ObserverInterface
{
    /**
     * @var \Ciratt\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @param \Ciratt\CustomLogger\Logger\Logger $logger
     */
    public function __construct(
        \Ciratt\CustomLogger\Logger\Logger $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * 
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Customer\Model\Customer $model */
        $model = $observer->getEvent()->getData('model');
        if($model->getId()) {
            $this->logger->info('TEST Custom Logger');
        }
    }
}