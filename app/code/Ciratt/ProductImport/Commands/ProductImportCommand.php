<?php

namespace Ciratt\ProductImport\Commands;

use Symfony\Component\Console\Input\{
    InputInterface,
    InputArgument,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\Data\CategoryInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Ciratt\ProductImport\Model\ReadCSV;
use Ciratt\ProductImport\Model\AssociatesProduct;
use Ciratt\CustomLogger\Logger\Logger;
use Ciratt\ProductImport\Model\CreateProduct;
use Ciratt\ProductImport\Model\CreateCategory;
use Ciratt\ProductImport\Model\CreateProductConf;
use Ciratt\ProductImport\Model\CreateAttribute;
use Magento\Indexer\Model\IndexerFactory;
use Magento\Indexer\Model\Indexer\CollectionFactory;

/**
 * ProductImporterCommand
 *
 * Exposed Magento command option that allows the caller to import products.
 */
class ProductImportCommand extends Command {

    /**
     * @var \Ciratt\ProductImport\Model\ReadCSV $readCSV
     */
    protected $readCSV;

    /**
     * @var \Ciratt\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var \Ciratt\CategoryImport\Model\CreateProduct $createProduct
     */
    protected $createProduct;

    /**
     * @var \Ciratt\CategoryImport\Model\CreateCategory $createCategory
     */
    protected $createCategory;

    /**
     * @var \Ciratt\CategoryImport\Model\createProductConf $createProductConf
     */
    protected $createProductConf;

    /**
     * @var \Ciratt\CategoryImport\Model\createAttribute $createAttribute
     */
    protected $createAttribute;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory $indexerFactory
     */
    protected $indexerFactory;

    /**
     * @var \Magento\Indexer\Model\Indexer\CollectionFactory $indexerCollectionFactory
     */
    protected $indexerCollectionFactory;

    /**
     * @var \Ciratt\CategoryImport\Model\createProductConf $sssociatesProduct
     */
    protected $associatesProduct;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(AssociatesProduct $associatesProduct, IndexerFactory $indexerFactory, CollectionFactory $indexerCollectionFactory, State $state, ProductRepositoryInterface $prepo, ReadCSV $readCSV, Logger $logger, CreateProduct $createProduct, CreateCategory $createCategory, CreateAttribute $createAttribute, CreateProductConf $createProductConf) {
        // We cannot use core functions (like saving a category) unless the area
        // code is explicitly set.
        try {
            if (!$state->getAreaCode()) {
                $state->setAreaCode('adminhtml');
            }
        } catch (\Exception $e) {
            // Intentionally left empty.
            // to check for error uncomment the following line
            // var_dump($e->getMessage());
        }
        $this->readCSV = $readCSV;
        $this->logger = $logger;
        $this->createProduct = $createProduct;
        $this->createCategory = $createCategory;
        $this->createProductConf = $createProductConf;
        $this->createAttribute = $createAttribute;
        $this->indexerFactory = $indexerFactory;
        $this->indexerCollectionFactory = $indexerCollectionFactory;
        $this->associatesProduct = $associatesProduct;
        parent::__construct();
    }

    /**
     * Configures arguments and display options for this command.
     *
     * @return void
     */
    protected function configure() {
        $this->setName('ciratt:productimports');
        $this->setDescription('Import products into Magento');
        // Next line will add a new required parameter to our script
        $this->addArgument('type', InputArgument::REQUIRED, __('Type all or qty'));
        parent::configure();
    }

    /**
     * Execute the command to add caegory data into the database.
     *
     * @param InputInterface $input An input instance
     * @param OutputInterface $output An output instance
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $type = $input->getArgument('type');
        $output->writeln('<info>Loading SKU information from ' . $type . '</info>');
        ini_set('memory_limit', '-1'); // or you could use 1G
        $sayAssociatedSku = array();
        $import_dir = '/srv/www/vhosts/pasticcinformatici/magento2.pasticcinformatici.com/website/updateCatalog/file/csv';
        $bck_dir = '/srv/www/vhosts/pasticcinformatici/magento2.pasticcinformatici.com/website/updateCatalog/file/csv_bck';
        $prRoot = '/var/www/vhosts/redrock.it/stage.redrock.it';

        if ($type == 'all' or $type == 'qty') {
            $to_import = scandir($import_dir);
            $to_import = array_diff($to_import, array('.', '..'));
            $output->writeln('<info>Inizio import update prodotti</info>');
            if (empty($to_import)) {
                $output->writeln('<info>' . date('d/m/Y H:i:s') . ' - Nessun file da importare</info>');
            } else {
                $csv_rows = $this->readCSV->sayHello($to_import, $import_dir, $output);
                $newRow = array();
                $newRowSimple = array();
                $key = 0;
                foreach ($csv_rows as $csv_row) {
                    $sku = $csv_row[0];
                    $output->writeln('Product Type Sku Command ' . $key . ' = <comment>' . $sku . '</comment>');
                    $desc = nl2br($csv_row[75]);
                    // $output->writeln('Product Type ' . $key . ' = <comment>' . $desc . '</comment>');
                    $name = $csv_row[1];
                    $output->writeln('Product Type Name Command ' . $key . ' = <comment>' . $name . '</comment>');
                    $price = nl2br($csv_row[2]);
                    // $output->writeln('Product Type ' . $key . ' = <comment>' . $price . '</comment>');
                    $specialPrice = nl2br($csv_row[3]);
                    // $output->writeln('Product Type ' . $key . ' = <comment>' . $specialPrice . '</comment>');
                    $cat = "[Redrock]/" . trim($csv_row[5]) . '/' . trim($csv_row[7]) . '/' . trim($csv_row[9]);
                    $output->writeln('Product Type Category Command ' . $key . ' = <comment>' . $cat . '</comment>');
                    $manufacturer = $csv_row[73];
                    // $output->writeln('Product Type ' . $key . ' = <comment>' . $manufacturer . '</comment>');
                    $store = 'italiano';
                    $website = 'base';
                    if ($csv_row[4] == '99') {
                        $store = 'de';
                        $website = 'grisport_de';
                        $cat = "[Grisport DE]/" . trim($csv_row[5]) . '/' . trim($csv_row[7]) . '/' . trim($csv_row[9]);
                        $output->writeln('Product Type Category Command ' . $key . ' = <comment>' . $cat . '</comment>');
                    }
                    if ($csv_row[4] == '89') {
                        $store = 'it';
                        $website = 'grisport_it';
                        $cat = "[Grisport IT]/" . trim($csv_row[5]) . '/' . trim($csv_row[7]) . '/' . trim($csv_row[9]);
                        $output->writeln('Product Type Category Command ' . $key . ' = <comment>' . $cat . '</comment>');
                    }

                    //configurabile
                    $dataArrivo = $csv_row[11];
                    if (preg_match("~s*~", $dataArrivo))
                        $dataArrivo = "01/01/0001";
                    $dataArrivo = explode('/', $dataArrivo);
                    if (strlen($dataArrivo[2]) == "2")
                        $dataArrivo[2] = "20" . $dataArrivo[2];
                    if (checkdate($dataArrivo[1], $dataArrivo[0], $dataArrivo[2])) {
                        $dataArrivo = $dataArrivo[0] . '-' . $dataArrivo[1] . '-' . $dataArrivo[2];
                        if (strtotime($dataArrivo) > time()) {
                            $dataArrivo = date('Y-m-d H:i:s', strtotime($dataArrivo));
                            $customDataArrivo = "Disponibile da " . date('Y-m-d H:i:s', strtotime($dataArrivo));
                            $hideStatus = "1";
                        } else {
                            $dataArrivo = "";
                            $customDataArrivo = "";
                            $hideStatus = "0";
                        }
                    } else {
                        $dataArrivo = "";
                        $customDataArrivo = "";
                        $hideStatus = "0";
                    }
                    $isInStock = 1;
                    if ($csv_row[10]) {
                        $isInStock = (intval($csv_row[10]) > 0) ? 1 : 0;
                    }
                    if ($type == 'all') {
                        $typeProd = 'simple';
                        $visibility = 4;
                        $configurable_attributes = 'size';
                        $output->writeln('Product Type ' . $typeProd . ' ' . $configurable_attributes . ' Command ' . $key . ' = <comment>' . $sku . '</comment>');
                        $newRow = array(
                            "sku" => $sku,
                            "store" => "admin," . $store,
                            "website" => $website,
                            "type" => $typeProd,
                            "attribute_set" => "Default",
                            "tax_class_id" => 2,
                            "visibility" => $visibility,
                            "status" => 1,
                            "configurable_attributes" => $configurable_attributes,
                            "weight" => 0,
                            "name" => $name,
                            "description" => $desc,
                            "short_description" => $desc,
                            "categories" => $cat,
                            "manufacturer" => $manufacturer,
                            "price" => $price,
                            "special_price" => $specialPrice,
                            "arrival_date" => $dataArrivo,
                            "custom_stock_status" => $customDataArrivo,
                            "hide_default_stock_status" => $hideStatus,
                            "qty" => ((intval($csv_row[10]) == 0) ? "0.00" : intval($csv_row[10])),
                            'is_in_stock' => $isInStock,
                            'ean' => '',
                            'color' => $csv_row[74]
                        );
                    }
                    if ($type == 'qty') {
                        $newRow = array(
                            "sku" => $sku,
                            "store" => "admin",
                            "tax_class_id" => 2,
                            "price" => $price,
                            "special_price" => $specialPrice,
                            "website" => $website,
                            "data_arrivo" => $dataArrivo,
                            "custom_stock_status" => $customDataArrivo,
                            "hide_default_stock_status" => $hideStatus,
                            "qty" => ((intval($csv_row[10]) == 0) ? "0.00" : intval($csv_row[10])),
                            'is_in_stock' => $isInStock,
                        );
                    }

                    $skuCategory = $sku;
                    $storeCategory = "admin," . $store;
                    $websiteCategory = $website;
                    $manufacturerAtt = $manufacturer;
                    $colorAtt = $csv_row[74];
                    $sizeAtt = '';
                    $seeHellosAtt = $this->createAttribute->seeHello($manufacturerAtt, $sizeAtt, $colorAtt, $skuCategory, $storeCategory, $websiteCategory, $key, $output);
                    $seeHellosCat = $this->createCategory->seeHello($cat, $skuCategory, $storeCategory, $key, $output);
                    $seeHellosConf = $this->createProductConf->seeHello($newRow, $type, $key, $output, $prRoot);
                    $skuConfig = '';
                    $key++;

                    //semplici
                    $n = 9;
                    for ($c = 0; $c <= 14; ++$c) {
                        $n = ($n + 4); // riga 13 taglia
                        $skuSimple = $csv_row[0] . '-' . $csv_row[$n];
                        $dataArrivoSemplice = $csv_row[$n + 2];
                        if (preg_match("~s*~", $dataArrivoSemplice))
                            $dataArrivoSemplice = "01/01/0001";
                        $dataArrivoSemplice = explode('/', $dataArrivoSemplice);
                        if (strlen($dataArrivoSemplice[2]) == "2")
                            $dataArrivoSemplice[2] = "20" . $dataArrivoSemplice[2];
                        if (checkdate($dataArrivoSemplice[1], $dataArrivoSemplice[0], $dataArrivoSemplice[2])) {
                            $dataArrivoSemplice = $dataArrivoSemplice[0] . '-' . $dataArrivoSemplice[1] . '-' . $dataArrivoSemplice[2];
                            if (strtotime($dataArrivoSemplice) > time()) {
                                $dataArrivoSemplice = date('Y-m-d H:i:s', strtotime($dataArrivoSemplice));
                                $customDataArrivoSemplice = "Disponibile da " . date('Y-m-d H:i:s', strtotime($dataArrivoSemplice));
                                $hideStatusSemplice = "1";
                            } else {
                                $dataArrivoSemplice = "";
                                $customDataArrivoSemplice = "";
                                $hideStatusSemplice = "0";
                            }
                        } else {
                            $dataArrivoSemplice = "";
                            $customDataArrivoSemplice = "";
                            $hideStatusSemplice = "0";
                        }

                        if (!empty($csv_row[$n])) {
                            if ($type == 'all') {
                                $sizeAtt = $csv_row[$n];
                                $skuConfig = $csv_row[0];
                                $configurable_attributes = 'size';
                                $output->writeln('Product Type Simple Size Command ' . $key . ' = <comment>' . $skuSimple . '</comment>');
                                $newRowSimple = array(
                                    "sku" => $skuSimple,
                                    "store" => "admin," . $store,
                                    "website" => $website,
                                    "type" => "simple",
                                    "attribute_set" => "Default",
                                    "tax_class_id" => 2,
                                    "visibility" => 1,
                                    "status" => 1,
                                    "weight" => 0,
                                    "name" => $name,
                                    "description" => $desc,
                                    "short_description" => $desc,
                                    "categories" => $cat,
                                    "manufacturer" => $manufacturer,
                                    "price" => $price,
                                    "special_price" => $specialPrice,
                                    "arrival_date" => $dataArrivoSemplice,
                                    "custom_stock_status" => $customDataArrivoSemplice,
                                    "hide_default_stock_status" => $hideStatusSemplice,
                                    "qty" => ((intval($csv_row[$n + 1]) == 0) ? "0.00" : intval($csv_row[$n + 1])),
                                    "is_in_stock" => (intval($csv_row[$n + 1]) > 0) ? 1 : 0,
                                    "ean" => $csv_row[$n + 3],
                                    "color" => $csv_row[74],
                                    "size" => $csv_row[$n],
                                    "configurable_attributes" => $configurable_attributes,
                                    "skuConfig" => $skuConfig
                                );
                            }
                            if ($type == 'qty') {
                                $newRowSimple = array(
                                    "sku" => $skuSimple,
                                    "tax_class_id" => 2,
                                    "price" => $price,
                                    "special_price" => $specialPrice,
                                    "store" => "admin",
                                    "qty" => ((intval($csv_row[$n + 1]) == 0) ? "0.00" : intval($csv_row[$n + 1])),
                                    "data_arrivo" => $dataArrivoSemplice,
                                    "custom_stock_status" => $customDataArrivoSemplice,
                                    "hide_default_stock_status" => $hideStatusSemplice,
                                    "is_in_stock" => (intval($csv_row[$n + 1]) > 0) ? 1 : 0,
                                );
                            }
                            $skuCategory = $skuSimple;
                            $storeCategory = "admin," . $store;
                            $websiteCategory = $website;
                            $manufacturerAtt = $manufacturer;
                            $colorAtt = $csv_row[74];
                            $seeHellosAtt = $this->createAttribute->seeHello($manufacturerAtt, $sizeAtt, $colorAtt, $skuCategory, $storeCategory, $websiteCategory, $key, $output);
                            $seeHellosCat = $this->createCategory->seeHello($cat, $skuCategory, $storeCategory, $key, $output);
                            $seeHellosSimp = $this->createProduct->seeHello($newRowSimple, $type, $key, $output);
                            $key++;
                        }
                    }

                    // taglie
                    $j = 13;
                    while ($j <= 69) {
                        if (!empty($csv_row[$j])) {
                            $skuAssociates = $csv_row[0] . '-' . $csv_row[$j];
                            $output->writeln('Product Type Simple Size Command ' . $key . ' = <comment>' . $j . ' / ' . $skuAssociates . '</comment>');
                            $seeHellosAssociates = $this->associatesProduct->seeHello($skuAssociates, $type, $key, $output);
                        }
                        $j = $j + 4;
                    }
                }
            }

            //reindex
            $output->writeln('<info>reindex begin</info>');
            $indexerCollection = $this->indexerCollectionFactory->create();
            $ids = $indexerCollection->getAllIds();
            foreach ($ids as $id) {
                $idx = $this->indexerFactory->create()->load($id);
                $idx->reindexAll($id); // this reindexes all
                $output->writeln('<info> reindex the end</info>');
            }
            $output->writeln('<info>Import the end</info>');
            /*
              $check = rename($import_dir . "/" . $csv_rows[1], $bck_dir . "/" . $csv_rows[1]);
              if ($check == false) {
              $output->writeln('<info>Unable to move file ' . $csv_filename . '</info>');
              }
             * 
             */
            $output->writeln('<info>Product Category imported!</info>');
        } else {
            $output->writeln('<info>Product Category not imported!</info>');
        }
    }

}
