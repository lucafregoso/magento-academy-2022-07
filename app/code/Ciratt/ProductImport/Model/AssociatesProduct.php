<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Ciratt\CustomLogger\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute as Attribute;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as Configurable;

class AssociatesProduct extends AbstractModel {

    /**
     * @var $list
     */
    private $list;

    /**
     * @var $categoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \SR\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * @var $product
     */
    protected $product;

    /**
     * @var $attributeRepository
     */
    protected $attributeRepository;

    /**
     * @var $attributeOptionManagement
     */
    protected $attributeOptionManagement;

    /**
     * @var $attributeRepositoryInterface
     */
    protected $attributeRepositoryInterface;

    /**
     * @var $attributeOptionLabel
     */
    protected $attributeOptionLabel;

    /**
     * @var $option
     */
    protected $option;

    /**
     * @var $storeFactory
     */
    protected $storeFactory;

    /**
     * @var $websiteFactory
     */
    protected $websiteFactory;

    /**
     * @var $websiteFactory
     */
    protected $productFactory;

    /**
     * @var $attribute
     */
    protected $attribute;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(Configurable $configurable, Attribute $attribute, ProductAttributeRepositoryInterface $attributeRepositoryInterface, ProductFactory $productFactory, WebsiteFactory $websiteFactory, StoreFactory $storeFactory, CategoryFactory $categoryFactory, Category $list, Logger $logger, ProductRepository $productRepository, Product $product, AttributeRepository $attributeRepository, AttributeOptionManagementInterface $attributeOptionManagement, AttributeOptionLabelInterface $attributeOptionLabel, Option $option) {
        $this->categoryFactory = $categoryFactory;
        $this->list = $list;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->option = $option;
        $this->attributeOptionLabel = $attributeOptionLabel;
        $this->storeFactory = $storeFactory;
        $this->websiteFactory = $websiteFactory;
        $this->productFactory = $productFactory;
        $this->attributeRepositoryInterface = $attributeRepositoryInterface;
        $this->attribute = $attribute;
        $this->configurable = $configurable;
    }

    public function seeHello($skuAssociates, $type, $key, $output) {

        $objectManager = ObjectManager::getInstance();
        $keys = $key + 1;
        $output->writeln('Product Count Simple ' . $key . ' = <comment>' . $keys . '</comment>');
        $output->writeln('Product Sku Simple ' . $key . ' = <comment>' . $skuAssociates . '</comment>');

        $output->writeln('<info>Importing Simple Product...</info>');
        if ($type == 'all') {
            $skuConfigAssociates = explode('-', $skuAssociates);
            $productConfigAssociatesId = $this->product->getIdBySku($skuConfigAssociates[0]);
            $attribute_id = $this->attributeRepository->get('catalog_product', 'size')->getAttributeId();
            if (!empty($productConfigAssociatesId)) {
                $output->writeln('productId  Configurable = <comment>' . $productConfigAssociatesId . '</comment>');
                $proudct_id = $this->product->getIdBySku($skuAssociates);
                $output->writeln('productId  Simple = <comment>' . $proudct_id . ' NULL </comment>');
                $proudct_id_array[] = $proudct_id;
                print_r($proudct_id_array);
                $configProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productConfigAssociatesId);

                $typeId = $configProduct->getTypeId();
                if ($typeId == "configurable") {
                    $output->writeln('Updated  = <comment> Configurable </comment>');
                    $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);
                    foreach ($_children as $child) {
                        $output->writeln('Here are your child Product Idse = <comment>' . $child->getId() . '</comment>');
                        $proudct_id_array[] = $child->getId();
                    }
                    $output->writeln('count = <comment>' . count($_children) . '</comment>');
                    $count_children = count($_children);
                    $count_position = $count_children + 1;

                    $associatedProductIds = $proudct_id_array;
                } else {
                    $output->writeln('Updated  = <comment> Simple </comment>');
                    $count_position = 1;
                    $associatedProductIds = $proudct_id_array;
                }

                $productss = $objectManager->create('Magento\Catalog\Model\Product')->load($productConfigAssociatesId); // Load Configurable Product
                $attributeModel = $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute');
                $position = $count_position;
                $attributeSizeId = $attribute_id;
                $attributeSetId = 4;

                $attributes = array($attributeSizeId);

                if ($typeId == "configurable") {
                    $attributess = ($productss->getTypeInstance(true)->getConfigurableAttributesAsArray($productss));
                    print_r($attributess);
                    if (count($attributes) == 0) {
                        foreach ($attributes as $attributeId) {
                            $data = array('attribute_id' => $attributeId, 'product_id' => $productConfigAssociatesId, 'position' => $position);
                            $position++;
                            $attributeModel->setData($data)->save();
                        }
                    }
                } else {
                    foreach ($attributes as $attributeId) {
                        $data = array('attribute_id' => $attributeId, 'product_id' => $productConfigAssociatesId, 'position' => $position);
                        $position++;
                        $attributeModel->setData($data)->save();
                    }
                    $productss->setTypeId("configurable");
                }

                $productss->setAffectConfigurableProductAttributes($attributeSetId);
                $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable')->setUsedProductAttributeIds($attributes, $productss);
                $productss->setNewVariationsAttributeSetId($attributeSetId);
                $productss->setAssociatedProductIds($associatedProductIds);
                $productss->setCanSaveConfigurableAttributes(true);
                $productss->save();

                unset($associatedProductIds);
            } else {
                $output->writeln('<info>Not Create Configurable Product...</info>');
            }
        } else {
            $output->writeln('<error>Error Simple Product...</error>');
        }
    }

}
