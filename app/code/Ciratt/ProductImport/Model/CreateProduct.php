<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Ciratt\CustomLogger\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute as Attribute;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as Configurable;

class CreateProduct extends AbstractModel {

    /**
     * @var $list
     */
    private $list;

    /**
     * @var $categoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \SR\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * @var $product
     */
    protected $product;

    /**
     * @var $attributeRepository
     */
    protected $attributeRepository;

    /**
     * @var $attributeOptionManagement
     */
    protected $attributeOptionManagement;

    /**
     * @var $attributeRepositoryInterface
     */
    protected $attributeRepositoryInterface;

    /**
     * @var $attributeOptionLabel
     */
    protected $attributeOptionLabel;

    /**
     * @var $option
     */
    protected $option;

    /**
     * @var $storeFactory
     */
    protected $storeFactory;

    /**
     * @var $websiteFactory
     */
    protected $websiteFactory;

    /**
     * @var $websiteFactory
     */
    protected $productFactory;

    /**
     * @var $attribute
     */
    protected $attribute;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(Configurable $configurable, Attribute $attribute, ProductAttributeRepositoryInterface $attributeRepositoryInterface, ProductFactory $productFactory, WebsiteFactory $websiteFactory, StoreFactory $storeFactory, CategoryFactory $categoryFactory, Category $list, Logger $logger, ProductRepository $productRepository, Product $product, AttributeRepository $attributeRepository, AttributeOptionManagementInterface $attributeOptionManagement, AttributeOptionLabelInterface $attributeOptionLabel, Option $option) {
        $this->categoryFactory = $categoryFactory;
        $this->list = $list;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->option = $option;
        $this->attributeOptionLabel = $attributeOptionLabel;
        $this->storeFactory = $storeFactory;
        $this->websiteFactory = $websiteFactory;
        $this->productFactory = $productFactory;
        $this->attributeRepositoryInterface = $attributeRepositoryInterface;
        $this->attribute = $attribute;
        $this->configurable = $configurable;
    }

    public function seeHello($newRowSimple, $type, $key, $output) {

        $objectManager = ObjectManager::getInstance();
        $keys = $key + 1;
        $output->writeln('Product Count Simple ' . $key . ' = <comment>' . $keys . '</comment>');
        /*
          $output->writeln('Product Sku Simple ' . $key . ' = <comment>' . $newRowSimple['sku'] . '</comment>');
          $output->writeln('Product Store Simple ' . $key . ' = <comment>' . $newRowSimple['store'] . '</comment>');
          $output->writeln('Product Type Simple ' . $key . ' = <comment>' . $newRowSimple['type'] . '</comment>');
          $output->writeln('Product Attribute Set Simple ' . $key . ' = <comment>' . $newRowSimple['attribute_set'] . '</comment>');
          $output->writeln('Product Tax Class Simple ' . $key . ' = <comment>' . $newRowSimple['tax_class_id'] . '</comment>');
          $output->writeln('Product Visibility Simple ' . $key . ' = <comment>' . $newRowSimple['visibility'] . '</comment>');
          $output->writeln('Product Status Simple ' . $key . ' = <comment>' . $newRowSimple['status'] . '</comment>');
          $output->writeln('Product Weight Simple ' . $key . ' = <comment>' . $newRowSimple['weight'] . '</comment>');
          $output->writeln('Product Name Simple ' . $key . ' = <comment>' . $newRowSimple['name'] . '</comment>');
          $output->writeln('Product Description Simple ' . $key . ' = <comment>' . $newRowSimple['description'] . '</comment>');
          $output->writeln('Product Short Description Simple ' . $key . ' = <comment>' . $newRowSimple['short_description'] . '</comment>');
          $output->writeln('Product Categories Simple ' . $key . ' = <comment>' . $newRowSimple['categories'] . '</comment>');
          $output->writeln('Product Manufacturer Simple ' . $key . ' = <comment>' . $newRowSimple['manufacturer'] . '</comment>');
          $output->writeln('Product Price Simple ' . $key . ' = <comment>' . $newRowSimple['price'] . '</comment>');
          $output->writeln('Product Special Price Simple ' . $key . ' = <comment>' . $newRowSimple['special_price'] . '</comment>');
          $output->writeln('Product Data Arrivo Simple ' . $key . ' = <comment>' . $newRowSimple['arrival_date'] . '</comment>');
          $output->writeln('Product Stock Status Custom Simple ' . $key . ' = <comment>' . $newRowSimple['custom_stock_status'] . '</comment>');
          $output->writeln('Product Stock Status Default Simple ' . $key . ' = <comment>' . $newRowSimple['hide_default_stock_status'] . '</comment>');
          $output->writeln('Product Qty Simple ' . $key . ' = <comment>' . $newRowSimple['qty'] . '</comment>');
          $output->writeln('Product In Stock Simple ' . $key . ' = <comment>' . $newRowSimple['is_in_stock'] . '</comment>');
          $output->writeln('Product EAN Simple ' . $key . ' = <comment>' . $newRowSimple['ean'] . '</comment>');
          $output->writeln('Product Color Simple ' . $key . ' = <comment>' . $newRowSimple['color'] . '</comment>');
          $output->writeln('Product Size Simple ' . $key . ' = <comment>' . $newRowSimple['size'] . '</comment>');
          $output->writeln('Product Configurable Attributes Simple ' . $key . ' = <comment>' . $newRowSimple['configurable_attributes'] . '</comment>');
          $output->writeln('Product Configurable Sku ' . $key . ' = <comment>' . $newRowSimple['skuConfig'] . '</comment>');
         */
        $output->writeln('<info>Importing Simple Product...</info>');
        if ($type == 'all') {
            $productId = $this->product->getIdBySku($newRowSimple['sku']);
            $output->writeln('productId  Simple = <comment>' . $productId . ' NULL </comment>');
            $attribute_size_id = $this->attributeRepository->get('catalog_product', 'size')->getAttributeId();
            $output->writeln('productId Simple New = <comment>' . $productId . ' NULL / Attribute Size : ' . $attribute_size_id . '</comment>');
            if ($productId == NULL) {
                $output->writeln('<info>Create Simple Product...</info>');
                $categorys = explode('/', $newRowSimple['categories']); // Category Names
                unset($categorys[0]);
                $categoryIdArray = array();
                foreach ($categorys as $category) {
                    $collection = $this->categoryFactory->create()->getCollection()->addAttributeToFilter('name', $category)->setPageSize(1);
                    $categoryId = $collection->getFirstItem()->getId();
                    $output->writeln('Readed category = <comment>' . $category . ' - ' . $categoryId . '</comment>');
                    array_push($categoryIdArray, $categoryId);
                }

                $store = $this->storeFactory->create();
                $storeCategory = explode(',', $newRowSimple['store']);
                $store->load($storeCategory[1]);
                $storeId = $store->getId();
                $output->writeln('Product Store ' . $key . ' = <comment>' . $storeId . '</comment>');

                $websiteIdArray = array();
                $website = $this->websiteFactory->create();
                $website->load($newRowSimple['website']);
                $websiteId = $website->getId();
                array_push($websiteIdArray, $websiteId);
                $output->writeln('Product Wesite ' . $key . ' = <comment>' . $websiteId . '</comment>');

                $product = $this->productFactory->create();
                $product->setStoreId($storeId);
                $product->setWebsiteIds($websiteIdArray);
                $product->setStatus(Status::STATUS_ENABLED);
                $product->setAttributeSetId($product->getDefaultAttributeSetId());
                $product->setVisibility($newRowSimple['visibility']);
                $product->setTaxClassId($newRowSimple['tax_class_id']);
                $product->setSku($newRowSimple['sku']);
                $product->setName($newRowSimple['name']);
                $product->setDescription($newRowSimple['description']);
                $product->setShortDescription($newRowSimple['short_description']);
                $product->setPrice($newRowSimple['price']);

                if ($newRowSimple['special_price'] != 0) {
                    $product->setSpecialPrice($newRowSimple['special_price']);
                }

                $product->setTypeId($newRowSimple['type']);
                $product->setCategoryIds($categoryIdArray);
                $product->setData('ean', $newRowSimple['ean']);

                // color
                if (!empty($newRowSimple['color'])) {
                    $attributecolor = $this->attributeRepositoryInterface->get('color');
                    $color_id = $attributecolor->getSource()->getOptionId($newRowSimple['color']); // get Xl option ID
                    $output->writeln('Product Simple New Color Id = <comment>' . $color_id . '</comment>');
                    $product->setColor($color_id);
                } else {
                    $output->writeln('<info>Not Create Simple Product Color...</info>');
                }

                // size
                if (!empty($newRowSimple['size'])) {
                    $attributesize = $this->attributeRepositoryInterface->get('size');
                    $size_id = $attributesize->getSource()->getOptionId($newRowSimple['size']); // get Xl option ID
                    $output->writeln('Product Simple New Size Id = <comment>' . $size_id . '</comment>');
                    $product->setSize($size_id);
                } else {
                    $output->writeln('<info>Not Create Simple Product Size...</info>');
                }

                // manufacturer
                if (!empty($newRowSimple['manufacturer'])) {
                    $attributemanufacturer = $this->attributeRepositoryInterface->get('manufacturer');
                    $manufacturer_id = $attributemanufacturer->getSource()->getOptionId($newRowSimple['manufacturer']); // get Xl option ID
                    $output->writeln('Product Simple New Manufacturer Id = <comment>' . $manufacturer_id . '</comment>');
                    $product->setManufacturer($manufacturer_id);
                } else {
                    $output->writeln('<info>Not Create Simple Product Manufacturer...</info>');
                }

                $product->setData('weight', $newRowSimple['weight']);
                $product->setMetaTitle($newRowSimple['name']);
                $product->setMetaDescription($newRowSimple['short_description']);
                $product->setMetaKeyword($newRowSimple['name']);

                $product->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                    'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => $newRowSimple['is_in_stock'], //Stock Availability
                    'qty' => $newRowSimple['qty'] //qty
                        )
                );

                $product->setData('arrival_date', $newRowSimple['arrival_date']);

                $product->save();

                unset($categoryIdArray);
                unset($websiteIdArray);

                $proudct_id = $product->getId();
                $output->writeln('productId Simple New = <comment>' . $proudct_id . ' / Size : ' . $newRowSimple['size'] . '</comment>');
                $product_sku = $product->getSku();
                $output->writeln('productSku Simple New = <comment>' . $product_sku . ' / Size : ' . $newRowSimple['size'] . '</comment>');

                unset($newRowSimple);
            } else {
                $output->writeln('<info>Not Create Simple Product...</info>');
            }
        } else if ($type == 'qty') {
            $output->writeln('<info>Update Simple Product...</info>');
            $model = $this->product->load($productId);

            $model->setTaxClassId($newRowSimple['tax_class_id']);
            $model->setPrice($newRowSimple['price']);

            if ($newRowSimple['special_price'] != 0) {
                $model->setSpecialPrice($newRowSimple['special_price']);
            }

            $model->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => $newRowSimple['is_in_stock'], //Stock Availability
                'qty' => $newRowSimple['qty'] //qty
                    )
            );
            $model->save();
        } else {
            $output->writeln('<error>Error Simple Product...</error>');
        }
    }

}
