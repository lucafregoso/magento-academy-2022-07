<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;

class ReadCSV extends AbstractModel {

    public function sayHello($to_import, $import_dir, $output) {
        $output->writeln('<info>Reading Row...</info>');
        foreach ($to_import as $csv_filename) {
            $output->writeln('<info>Reading Category...</info>');
            // Nome del file
            $filename = $csv_filename;
            // Percorso da cui prelevare il file
            $path = $import_dir;
            // File completo di percorso
            $file = $path . '/' . $filename;
            $output->writeln('<info>Parsing del file ' . $file . ' the end.');
            // Controllo se il file è leggibile
            if (!is_readable($file)) {
                $output->writeln('<info>The file is not readable or does not exist!</info>');
            } else {
                $file_handle = fopen($file, 'r');
                while (!feof($file_handle)) {
                    $line_of_text[] = fgetcsv($file_handle, 1024, ";");
                }
                fclose($file_handle);
                unset($line_of_text[0]);
                $output->writeln('<info>Parsing del file ' . $csv_filename . ' the end.</info>');
                return $line_of_text;
                // return array( '0' => $line_of_text , '1' => $csv_filename) ;
            }
        }
    }

}
