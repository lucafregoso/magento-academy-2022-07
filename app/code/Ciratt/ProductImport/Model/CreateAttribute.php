<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Ciratt\CustomLogger\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;

class CreateAttribute extends AbstractModel {

    /**
     * @var $list
     */
    private $list;

    /**
     * @var $categoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \SR\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * @var $product
     */
    protected $product;

    /**
     * @var $attributeRepository
     */
    protected $attributeRepository;

    /**
     * @var $attributeOptionManagement
     */
    protected $attributeOptionManagement;

    /**
     * @var $attributeRepositoryInterface
     */
    protected $attributeRepositoryInterface;

    /**
     * @var $attributeOptionLabel
     */
    protected $attributeOptionLabel;

    /**
     * @var $option
     */
    protected $option;

    /**
     * @var $storeFactory
     */
    protected $storeFactory;

    /**
     * @var $websiteFactory
     */
    protected $websiteFactory;

    /**
     * @var $websiteFactory
     */
    protected $productFactory;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(ProductAttributeRepositoryInterface $attributeRepositoryInterface, ProductFactory $productFactory, WebsiteFactory $websiteFactory, StoreFactory $storeFactory, CategoryFactory $categoryFactory, Category $list, Logger $logger, ProductRepository $productRepository, Product $product, AttributeRepository $attributeRepository, AttributeOptionManagementInterface $attributeOptionManagement, AttributeOptionLabelInterface $attributeOptionLabel, Option $option) {
        $this->categoryFactory = $categoryFactory;
        $this->list = $list;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->option = $option;
        $this->attributeOptionLabel = $attributeOptionLabel;
        $this->storeFactory = $storeFactory;
        $this->websiteFactory = $websiteFactory;
        $this->productFactory = $productFactory;
        $this->attributeRepositoryInterface = $attributeRepositoryInterface;
    }

    public function seeHello($manufacturerAtt, $sizeAtt, $colorAtt, $skuCategory, $storeCategory, $websiteCategory, $key, $output) {

        $keys = $key + 1;
        $output->writeln('Product Count Attribute ' . $key . ' = <comment>' . $keys . '</comment>');
        /*
          $output->writeln('Product Attribute Manufacturer ' . $key . ' = <comment>' . $manufacturerAtt . '</comment>');
          $output->writeln('Product Attribute Size ' . $key . ' = <comment>' . $sizeAtt . '</comment>');
          $output->writeln('Product Attribute Color ' . $key . ' = <comment>' . $colorAtt . '</comment>');
          $output->writeln('Product Sku ' . $key . ' = <comment>' . $skuCategory . '</comment>');
          $output->writeln('Product Store ' . $key . ' = <comment>' . $storeCategory . '</comment>');
         */

        $output->writeln('<info>Importing Configurable Product...</info>');
        $store = $this->storeFactory->create();
        $storeCategory = explode(',', $storeCategory);
        $store->load($storeCategory[1]);
        $storeId = $store->getId();
        $output->writeln('Product Store ' . $key . ' = <comment>' . $storeId . '</comment>');

        $websiteIdArray = array();
        $website = $this->websiteFactory->create();
        $website->load($websiteCategory);
        $websiteId = $website->getId();
        array_push($websiteIdArray, $websiteId);
        $output->writeln('Product Wesite ' . $key . ' = <comment>' . $websiteId . '</comment>');

        // color
        if (!empty($colorAtt)) {
            $attribute_color_id = $this->attributeRepository->get('catalog_product', 'color')->getAttributeId();
            $color_options = $this->attributeOptionManagement->getItems('catalog_product', $attribute_color_id);

            $output->writeln('Configurable Product attribute_id Color = <comment>' . $attribute_color_id . '</comment>');
            foreach ($color_options as $color_option) {
                // $output->writeln('Attribute Configurable Color = <comment>' . $color_option->getLabel() . '</comment>');
                $label_color_array[] = $color_option->getLabel();
                $value_color_array[] = $color_option->getValue();
            }

            $count_color_value = count($value_color_array);
            $color_newCount = $count_color_value + 1;

            if (in_array(strtolower($colorAtt), array_map('strtolower', $label_color_array))) {
                $output->writeln('Product Configurable Color = <comment>' . $colorAtt . '</comment>');
            } else {
                $output->writeln('Product Configurable  New Color = <comment>' . $colorAtt . '</comment>');
                $this->option->setValue(ucwords($colorAtt));
                $this->attributeOptionLabel->setStoreId($storeId);
                $this->attributeOptionLabel->setLabel(ucwords($colorAtt));
                $this->option->setLabel($this->attributeOptionLabel->getLabel());
                $this->option->setStoreLabels([$this->attributeOptionLabel]);
                $this->option->setSortOrder($color_newCount);
                $this->option->setIsDefault(false);
                $this->attributeOptionManagement->add('catalog_product', $attribute_color_id, $this->option);
            }
        } else {
            $output->writeln('<info>Not Create Configurable Product Color...</info>');
        }


        // size
        if (!empty($sizeAtt)) {
            $attribute_size_id = $this->attributeRepository->get('catalog_product', 'size')->getAttributeId();
            $size_options = $this->attributeOptionManagement->getItems('catalog_product', $attribute_size_id);

            $output->writeln('Configurable Product attribute_id Size = <comment>' . $attribute_size_id . '</comment>');
            foreach ($size_options as $size_option) {
                // $output->writeln('Attribute Configurable Size = <comment>' . $size_option->getLabel() . '</comment>');
                $label_size_array[] = $size_option->getLabel();
                $value_size_array[] = $size_option->getValue();
            }

            $count_size_value = count($value_size_array);
            $size_newCount = $count_size_value + 1;

            if (in_array(strtolower($sizeAtt), array_map('strtolower', $label_size_array))) {
                $output->writeln('Product Configurable Size = <comment>' . $sizeAtt . '</comment>');
            } else {
                $output->writeln('Product Configurable  New Size = <comment>' . $sizeAtt . '</comment>');
                $this->option->setValue(ucwords($sizeAtt));
                $this->attributeOptionLabel->setStoreId($storeId);
                $this->attributeOptionLabel->setLabel(ucwords($sizeAtt));
                $this->option->setLabel($this->attributeOptionLabel->getLabel());
                $this->option->setStoreLabels([$this->attributeOptionLabel]);
                $this->option->setSortOrder($size_newCount);
                $this->option->setIsDefault(false);
                $this->attributeOptionManagement->add('catalog_product', $attribute_size_id, $this->option);
            }
        } else {
            $output->writeln('<info>Not Create Configurable Product Size...</info>');
        }

        // manufacturer
        if (!empty($manufacturerAtt)) {
            $attribute_manufacturer_id = $this->attributeRepository->get('catalog_product', 'manufacturer')->getAttributeId();
            $manufacturer_options = $this->attributeOptionManagement->getItems('catalog_product', $attribute_manufacturer_id);

            $output->writeln('Configurable Product attribute_id Manufacturer = <comment>' . $attribute_manufacturer_id . '</comment>');
            foreach ($manufacturer_options as $manufacturer_option) {
                // $output->writeln('Attribute Configurable Manufacturer = <comment>' . $manufacturer_option->getLabel() . '</comment>');
                $label_manufacturer_array[] = $manufacturer_option->getLabel();
                $value_manufacturer_array[] = $manufacturer_option->getValue();
            }

            $count_manufacturer_value = count($value_manufacturer_array);
            $manufacturer_newCount = $count_manufacturer_value + 1;

            if (in_array(strtolower($manufacturerAtt), array_map('strtolower', $label_manufacturer_array))) {
                $output->writeln('Product Configurable Manufacturer = <comment>' . $manufacturerAtt . '</comment>');
            } else {
                $output->writeln('Product Configurable  New Manufacturer = <comment>' . $manufacturerAtt . '</comment>');
                $this->option->setValue(ucwords($manufacturerAtt));
                $this->attributeOptionLabel->setStoreId($storeId);
                $this->attributeOptionLabel->setLabel(ucwords($manufacturerAtt));
                $this->option->setLabel($this->attributeOptionLabel->getLabel());
                $this->option->setStoreLabels([$this->attributeOptionLabel]);
                $this->option->setSortOrder($manufacturer_newCount);
                $this->option->setIsDefault(false);
                $this->attributeOptionManagement->add('catalog_product', $attribute_manufacturer_id, $this->option);
            }
        } else {
            $output->writeln('<info>Not Create Configurable Product Manufacturer...</info>');
        }
    }

}
