<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Ciratt\CustomLogger\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;

class CreateProductConf extends AbstractModel {

    /**
     * @var $list
     */
    private $list;

    /**
     * @var $categoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \SR\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * @var $product
     */
    protected $product;

    /**
     * @var $attributeRepository
     */
    protected $attributeRepository;

    /**
     * @var $attributeOptionManagement
     */
    protected $attributeOptionManagement;

    /**
     * @var $attributeRepositoryInterface
     */
    protected $attributeRepositoryInterface;

    /**
     * @var $attributeOptionLabel
     */
    protected $attributeOptionLabel;

    /**
     * @var $option
     */
    protected $option;

    /**
     * @var $storeFactory
     */
    protected $storeFactory;

    /**
     * @var $websiteFactory
     */
    protected $websiteFactory;

    /**
     * @var $websiteFactory
     */
    protected $productFactory;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(ProductAttributeRepositoryInterface $attributeRepositoryInterface, ProductFactory $productFactory, WebsiteFactory $websiteFactory, StoreFactory $storeFactory, CategoryFactory $categoryFactory, Category $list, Logger $logger, ProductRepository $productRepository, Product $product, AttributeRepository $attributeRepository, AttributeOptionManagementInterface $attributeOptionManagement, AttributeOptionLabelInterface $attributeOptionLabel, Option $option) {
        $this->categoryFactory = $categoryFactory;
        $this->list = $list;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->option = $option;
        $this->attributeOptionLabel = $attributeOptionLabel;
        $this->storeFactory = $storeFactory;
        $this->websiteFactory = $websiteFactory;
        $this->productFactory = $productFactory;
        $this->attributeRepositoryInterface = $attributeRepositoryInterface;
    }

    public function seeHello($newRow, $type, $key, $output, $prRoot) {

        $keys = $key + 1;
        $output->writeln('Product Count Configurable ' . $key . ' = <comment>' . $keys . '</comment>');
        /*
          $output->writeln('Product Sku Configurable ' . $key . ' = <comment>' . $newRow['sku'] . '</comment>');
          $output->writeln('Product Store Configurable ' . $key . ' = <comment>' . $newRow['store'] . '</comment>');
          $output->writeln('Product Store Configurable ' . $key . ' = <comment>' . $newRow['website'] . '</comment>');
          $output->writeln('Product Type Configurable ' . $key . ' = <comment>' . $newRow['type'] . '</comment>');
          $output->writeln('Product Attribute Set Configurable ' . $key . ' = <comment>' . $newRow['attribute_set'] . '</comment>');
          $output->writeln('Product Tax Class Configurable ' . $key . ' = <comment>' . $newRow['tax_class_id'] . '</comment>');
          $output->writeln('Product Visibility Configurable ' . $key . ' = <comment>' . $newRow['visibility'] . '</comment>');
          $output->writeln('Product Status Configurable ' . $key . ' = <comment>' . $newRow['status'] . '</comment>');
          $output->writeln('Product Configurable Attributes Configurable ' . $key . ' = <comment>' . $newRow['configurable_attributes'] . '</comment>');
          $output->writeln('Product Weight Configurable ' . $key . ' = <comment>' . $newRow['weight'] . '</comment>');
          $output->writeln('Product Name Configurable ' . $key . ' = <comment>' . $newRow['name'] . '</comment>');
          $output->writeln('Product Description Configurable ' . $key . ' = <comment>' . $newRow['description'] . '</comment>');
          $output->writeln('Product Short Description Configurable ' . $key . ' = <comment>' . $newRow['short_description'] . '</comment>');
          $output->writeln('Product Categories Configurable ' . $key . ' = <comment>' . $newRow['categories'] . '</comment>');
          $output->writeln('Product Manufacturer Configurable ' . $key . ' = <comment>' . $newRow['manufacturer'] . '</comment>');
          $output->writeln('Product Price Configurable ' . $key . ' = <comment>' . $newRow['price'] . '</comment>');
          $output->writeln('Product Special Price Configurable ' . $key . ' = <comment>' . $newRow['special_price'] . '</comment>');
          $output->writeln('Product Data Arrivo Configurable ' . $key . ' = <comment>' . $newRow['arrival_date'] . '</comment>');
          $output->writeln('Product Stock Status Custom Configurable ' . $key . ' = <comment>' . $newRow['custom_stock_status'] . '</comment>');
          $output->writeln('Product Stock Status Default Configurable ' . $key . ' = <comment>' . $newRow['hide_default_stock_status'] . '</comment>');
          $output->writeln('Product Qty Configurable ' . $key . ' = <comment>' . $newRow['qty'] . '</comment>');
          $output->writeln('Product In Stock Configurable ' . $key . ' = <comment>' . $newRow['is_in_stock'] . '</comment>');
          $output->writeln('Product EAN Configurable ' . $key . ' = <comment>' . $newRow['ean'] . '</comment>');
          $output->writeln('Product Color Configurable ' . $key . ' = <comment>' . $newRow['color'] . '</comment>');
         */
        $output->writeln('<info>Importing Configurable Product...</info>');
        if ($type == 'all') {
            $productId = $this->product->getIdBySku($newRow['sku']);
            $output->writeln('productId  Configurable NULL = <comment>' . $productId . '</comment>');
            if ($productId == NULL) {
                $output->writeln('<info>Create Configurable Product...</info>');
                $categorys = explode('/', $newRow['categories']); // Category Names
                unset($categorys[0]);
                $categoryIdArray = array();
                foreach ($categorys as $category) {
                    $collection = $this->categoryFactory->create()->getCollection()->addAttributeToFilter('name', $category)->setPageSize(1);
                    $categoryId = $collection->getFirstItem()->getId();
                    $output->writeln('Readed category = <comment>' . $category . ' - ' . $categoryId . '</comment>');
                    array_push($categoryIdArray, $categoryId);
                }

                $store = $this->storeFactory->create();
                $storeCategory = explode(',', $newRow['store']);
                $store->load($storeCategory[1]);
                $storeId = $store->getId();
                $output->writeln('Product Store ' . $key . ' = <comment>' . $storeId . '</comment>');

                $websiteIdArray = array();
                $website = $this->websiteFactory->create();
                $website->load($newRow['website']);
                $websiteId = $website->getId();
                array_push($websiteIdArray, $websiteId);
                $output->writeln('Product Wesite ' . $key . ' = <comment>' . $websiteId . '</comment>');

                $product = $this->productFactory->create();
                $product->setStoreId($storeId);
                $product->setWebsiteIds($websiteIdArray);
                $product->setStatus(Status::STATUS_ENABLED);
                $product->setAttributeSetId($product->getDefaultAttributeSetId());
                $product->setVisibility($newRow['visibility']);
                $product->setTaxClassId($newRow['tax_class_id']);
                $product->setSku($newRow['sku']);
                $product->setName($newRow['name']);
                $name = $newRow['name'] . '-' . $newRow['sku'];
                $url = preg_replace('#[^0-9a-z]+#i', '-', $name);
                $url = strtolower($url);
                $product->setUrlKey($url);
                $product->setDescription($newRow['description']);
                $product->setShortDescription($newRow['short_description']);
                $product->setPrice($newRow['price']);

                if ($newRow['special_price'] != 0) {
                    $product->setSpecialPrice($newRow['special_price']);
                }

                $product->setTypeId($newRow['type']);
                $product->setCategoryIds($categoryIdArray);
                $product->setData('ean', $newRow['ean']);

                // color
                if (!empty($newRow['color'])) {
                    $attributecolor = $this->attributeRepositoryInterface->get('color');
                    $color_id = $attributecolor->getSource()->getOptionId($newRow['color']); // get Xl option ID
                    $output->writeln('Product Configurable New Color Id = <comment>' . $color_id . '</comment>');
                    $product->setColor($color_id);
                } else {
                    $output->writeln('<info>Not Create Configurable Product Color...</info>');
                }

                // size
                if (!empty($newRow['size'])) {
                    $attributesize = $this->attributeRepositoryInterface->get('size');
                    $size_id = $attributesize->getSource()->getOptionId($newRow['size']); // get Xl option ID
                    $output->writeln('Product Configurable New Size Id = <comment>' . $size_id . '</comment>');
                    $product->setSize($size_id);
                } else {
                    $output->writeln('<info>Not Create Configurable Product Size...</info>');
                }

                // manufacturer
                if (!empty($newRow['manufacturer'])) {
                    $attributemanufacturer = $this->attributeRepositoryInterface->get('manufacturer');
                    $manufacturer_id = $attributemanufacturer->getSource()->getOptionId($newRow['manufacturer']); // get Xl option ID
                    $output->writeln('Product Configurable New Manufacturer Id = <comment>' . $manufacturer_id . '</comment>');
                    $product->setManufacturer($manufacturer_id);
                } else {
                    $output->writeln('<info>Not Create Configurable Product Manufacturer...</info>');
                }

                $product->setData('weight', $newRow['weight']);
                $product->setMetaTitle($newRow['name']);
                $product->setMetaDescription($newRow['short_description']);
                $product->setMetaKeyword($newRow['name']);

                $product->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                    'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => $newRow['is_in_stock'], //Stock Availability
                    'qty' => $newRow['qty'] //qty
                        )
                );

                $product->setData('arrival_date', $newRow['arrival_date']);

                $sayHellosImgs = explode('-', $newRow['sku']);
                $sayHellosImgs = $sayHellosImgs[0] . '.jpg';
                $output->writeln('Product Configurable Image = <comment>' . $sayHellosImgs . '</comment>');
                $bck_dir = $prRoot . '/pub/media/product';
                if (file_exists($bck_dir . '/' . $sayHellosImgs)) {
                    $output->writeln('The file exists = <comment>' . $sayHellosImgs . '</comment>');
                    $product->addImageToMediaGallery('product/' . $sayHellosImgs, array('image', 'small_image', 'thumbnail'), false, false);
                } else {
                    $output->writeln('The file does not exist = <comment>' . $sayHellosImgs . '</comment>');
                }

                $product->save();
                
                unset($categoryIdArray);
                unset($websiteIdArray);
                
                $proudct_id = $product->getId();
                $output->writeln('productId Configurable New = <comment>' . $proudct_id .  '</comment>');
                $product_sku = $product->getSku();
                $output->writeln('productSku Configurable New = <comment>' . $product_sku . '</comment>');
                
                unset($newRow);
            } else {
                $output->writeln('<info>Not Create Configurable Product...</info>');
            }
        } else if ($type == 'qty') {
            $output->writeln('<info>Update Configurable Product...</info>');
            $model = $this->product->load($productId);

            $model->setTaxClassId($newRow['tax_class_id']);
            $model->setPrice($newRow['price']);

            if ($newRow['special_price'] != 0) {
                $model->setSpecialPrice($newRow['special_price']);
            }

            $model->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => $newRow['is_in_stock'], //Stock Availability
                'qty' => $newRow['qty'] //qty
                    )
            );
            $model->save();
        } else {
            $output->writeln('<error>Error Configurable Product...</error>');
        }
    }

}
