<?php

namespace Ciratt\ProductImport\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\{
    ObjectManager,
    State
};
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Ciratt\CustomLogger\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterface;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Store\Model\StoreFactory;

class CreateCategory extends AbstractModel {

    /**
     * @var $list
     */
    private $list;

    /**
     * @var $categoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \SR\CustomLogger\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * @var $product
     */
    protected $product;

    /**
     * @var $attributeRepository
     */
    protected $attributeRepository;

    /**
     * @var $attributeOptionManagement
     */
    protected $attributeOptionManagement;

    /**
     * @var $attributeOptionLabel
     */
    protected $attributeOptionLabel;

    /**
     * @var $option
     */
    protected $option;

    /**
     * @var $storeFactory
     */
    protected $storeFactory;

    /**
     * Constructor
     *
     * @param State $state A Magento app State instance
     *
     * @return void
     */
    public function __construct(StoreFactory $storeFactory, CategoryFactory $categoryFactory, Category $list, Logger $logger, ProductRepository $productRepository, Product $product, AttributeRepository $attributeRepository, AttributeOptionManagementInterface $attributeOptionManagement, AttributeOptionLabelInterface $attributeOptionLabel, Option $option) {
        $this->categoryFactory = $categoryFactory;
        $this->list = $list;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->option = $option;
        $this->attributeOptionLabel = $attributeOptionLabel;
        $this->storeFactory = $storeFactory;
    }

    public function seeHello($cat, $skuCategory, $storeCategory, $key, $output) {

        $keys = $key + 1;
        $output->writeln('Product Count Category ' . $key . ' = <comment>' . $keys . '</comment>');
        /*
          $output->writeln('Product Category ' . $key . ' = <comment>' . $cat . '</comment>');
          $output->writeln('Product Sku ' . $key . ' = <comment>' . $skuCategory . '</comment>');
          $output->writeln('Product Store ' . $key . ' = <comment>' . $storeCategory . '</comment>');
         */

        $store = $this->storeFactory->create();
        $storeCategory = explode(',', $storeCategory);
        $store->load($storeCategory[1]);
        $storeId = $store->getId();
        $output->writeln('Product Store ' . $key . ' = <comment>' . $storeId . '</comment>');
        $rootNodeId = $store->getRootCategoryId();
        $output->writeln('rootNodeId category = <comment>' . $rootNodeId . '</comment>');
        $rootCat = $this->categoryFactory->create();
        $cat_info = $rootCat->load($rootNodeId);

        $categorys = explode('/', $cat); // Category Names
        unset($categorys[0]);
        foreach ($categorys as $j => $category) {
            $output->writeln('key category = <comment>' . $j . '</comment>');
            $output->writeln('name category = <comment>' . $category . '</comment>');
            $name = ucfirst($category);
            $url = strtolower($category);
            $cleanurl = trim(preg_replace('/ +/', '', preg_replace('/[^A-Za-z0-9 ]/', '', urldecode(html_entity_decode(strip_tags($url))))));

            // Add a new sub category under root category
            $categoryTmp = $this->categoryFactory->create();
            $categoryTmp->setName($name);
            $categoryTmp->setIsActive(true);
            $categoryTmp->setUrlKey($cleanurl);
            if ($j == 1) {
                $ParentId = $rootCat->getId();
                $Path = $rootCat->getPath();
                $output->writeln('Path = <comment>' . $Path . '</comment>');
                $rootCategory = $category;
                $output->writeln('root category = <comment>' . $rootCategory . '</comment>');
            } else {
                $output->writeln('root category before = <comment>' . $rootCategory . '</comment>');
                $ParentId = $categoryId;
                $rootPath = $rootCat->load($ParentId);
                $Path = $rootPath->getPath();
                $output->writeln('Path = <comment>' . $Path . '</comment>');
                $rootCategory = $category;
                $output->writeln('root category after = <comment>' . $rootCategory . '</comment>');
            }
            $categoryTmp->setParentId($ParentId);
            $categoryTmp->setStoreId($storeId);
            $categoryTmp->setPath($Path);

            //Check exist category
            $collection = $this->categoryFactory->create()->getCollection()->addAttributeToFilter('name', $name)->setPageSize(1);
            if ($collection->getSize() || $name == NULL) {
                $categoryId = $collection->getFirstItem()->getId();
                $categoryPath = $collection->getFirstItem()->getPath();
                $output->writeln('View exist category = <comment>' . $name . ' - ' . $categoryId . ' - ' . $categoryPath . '</comment>');
            } else {
                $categoryTmp->save();
                $categoryPath = $collection->getFirstItem()->getPath();
                $output->writeln('Created category = <comment>' . $name . ' - ' . $categoryPath . '</comment>');
                $categoryId = $collection->getFirstItem()->getId();
                $output->writeln('Parent created category = <comment>' . $name . ' - ' . $categoryId . '</comment>');
            }
        }
        unset($categorys);
    }

}
