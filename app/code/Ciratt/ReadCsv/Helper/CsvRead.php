<?php

namespace Ciratt\ReadCsv\Helper;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;
use Symfony\Component\Console\Output\OutputInterface;

class CsvRead extends AbstractModel {

    public function sayHello($csvFile, $output) {
        $output->writeln('<info>Reading Category...</info>');
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 1024, ';');
        }
        fclose($file_handle);
        unset($line_of_text[0]);
        return $line_of_text;
    }

}