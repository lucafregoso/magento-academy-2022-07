<?php

namespace Ciratt\HelloWorld\Block;


use Magento\Framework\View\Element\Template;
use Ciratt\Core\Helper\Data;

class Test extends Template
{
        protected $helperData;
        
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Cms\Api\BlockRepositoryInterface $blockRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        Data $helperData,
        array $data = []
    ) {
        $this->blockRepository = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->helperData = $helperData;
        parent::__construct($context, $data);
    }
	
    public function getText() {
        return "Hello World Test Ciratt!";
    }
	
	/* Get Cms Blocks Collection from store. */
    public function getCmsBlock() {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $cmsBlocks = $this->blockRepository->getList($searchCriteria)->getItems();
        return $cmsBlocks;
    }
    
     public function getGeneralConfigParam($option) {
         return $this->helperData->getGeneralConfig($option);
    }
}