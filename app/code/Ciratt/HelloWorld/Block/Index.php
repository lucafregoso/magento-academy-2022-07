<?php

namespace Ciratt\HelloWorld\Block;


use Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function getText() {
        return "Hello World Index Ciratt!";
    }
}