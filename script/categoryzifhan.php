<?php

namespace HRM\ZhifanImportCSVCommand\Console\Command;

use HRM\ZhifanImportCSVCommand\PHPandas\DataFrame\DataFrame;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Store\Model\StoreManagerInterface as MyStoreManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCSVCommand extends Command
{
    private $_productFactory;
    private $_categoryLinkManagement;
    private $_categoryCollectionFactory;
    private $_categoryFactory;
    // private $_storeFactory;
    private $_storeManager;
    private $_myStoreManager;

    const PATH_CSV = "path_csv";
    // const NAME_OPTION = "option";

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        MyStoreManager $myStoreManager,
        \Magento\Framework\App\State $state
    ) {
        
        // We cannot use core functions (like saving a category) unless the area
        // code is explicitly set.
        try {
            // if (!$state->getAreaCode()) {
                $state->setAreaCode('adminhtml');
            // }
        } catch (\Exception $e) {
            // Intentionally left empty.
            // to check for error uncomment the following line
            // var_dump($e->getMessage());
        }
        $this->_productFactory = $productFactory;
        $this->_categoryLinkManagement = $categoryLinkManagement;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        // $this->_storeFactory = $storeFactory;
        $this->_myStoreManager = $myStoreManager;
        $this->_storeManager = $storeManager;
        parent::__construct();
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $pathCsv = $input->getArgument(self::PATH_CSV);
        $output->writeln("Importing $pathCsv");

        $dataframe = DataFrame::loadFromCSV($pathCsv);

        $categoryCollection = $this->_categoryCollectionFactory->create();
        /**
         * SKU (CODICE ARTICOLO);DESCRIZIONE (NOME ARTICOLO);FAMIGLIA;SOTTO-FAMIGLIA;GRUPPO;SOTTOGRUPPO;;;;DIMENSIONI;MATERIALE;COLORE;PESO NETTO;PESO LORDO;VOLUME;GIACENZA STONES;IMPEGNATO STONES;GIACENZA VOL;IMPEGNATO VOL;
         */
        // $stores = $this->_storeFactory->create();
        echo '<pre>';
        // var_dump($this->_storeManager);
        print_r($this->_storeManager->getStore()->getWebsiteId());
        // print_r($this->_storeManager->getStore()->getId());
        echo '</pre>';
        // die();

        $categories = $categoryCollection->addAttributeToSelect('name')->addAttributeToSelect('id');

        $productCategories = [];
        $storeIds = [0, 1, 2];

        foreach ($categories as $category) {
            $productCategories[$category->getName()] = $category->getId();

            // $productCategories[$category->getName()] = [
            //     'store_id' => $this->_storeManager->getStore()->getId(),
            //     'category' => $category->getId(),
            // ];
        }

        echo '<pre>';
        print_r($productCategories);
        echo '</pre>';
        // die();

        foreach ($dataframe as $row) {
            $productFactory = $this->_productFactory->create();

            $productSku = $row->getByColumnName('SKU (CODICE ARTICOLO)');

            if (is_null($productSku)) {
                continue;
            }

            $productName = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productDescription = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productShortDescription = $row->getByColumnName('DESCRIZIONE (NOME ARTICOLO)');
            $productPrice = $row->getByColumnName('PREZZO', 0.0);
            $productCategoryIds = [];
            $categoryNames = [
                $row->getByColumnName('FAMIGLIA'),
                $row->getByColumnName('SOTTO-FAMIGLIA'),
                $row->getByColumnName('GRUPPO'),
                $row->getByColumnName('SOTTOGRUPPO'),
            ];

            print("Processing name = $productName, SKU = $productSku, price = $productPrice\n");

            foreach ($categoryNames as $categoryName) {
                $category = $this->_categoryFactory->create();
                $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name', $categoryName)->setPageSize(1);
                if ($collection->getSize() || $categoryName == NULL) {
                    print("Creating new category '{$categoryName}'");
                    
                }
                if (isset($productCategories[$categoryName])) {
                    $productCategoryIds[] = $productCategories[$categoryName];
                    $n = rand(0, count($storeIds) - 1);
                    // print("n = '$n'\n")
                    $category->setStoreId($storeIds[$n]);
                    die();
                } else {
                    print("Creating new category '{$categoryName}'");
                    $category->setName($categoryName);
                    $category->setIsActive(true);
                    $category->setUrlKey($this->slugify($categoryName));
                    $category->setStoreId('1');
                    $category->save();
                    $productCategoryIds[$categoryName] = $category->getId();

                    print(" with ID '{$category->getId()}'\n");
                }
            }

            $productId = $productFactory->getIdBySku($productSku);

            if ($productId) {
                $productCategoryIds = $productFactory->getCategoryIds();
                $productFactory = $productFactory->load($productId);
            }

            // Visibility Type:
            // VISIBILITY_NOT_VISIBLE =>  1
            // VISIBILITY_IN_CATALOG =>  2
            // VISIBILITY_IN_SEARCH =>  3
            // VISIBILITY_BOTH =>  4
            $productFactory->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
            $productFactory->setSku($productSku);
            $productFactory->setStatus(Status::STATUS_ENABLED);
            $productFactory->setName($productName);
            $productFactory->setDescription($productDescription);
            $productFactory->setShortDescription($productShortDescription);
            $productFactory->setPrice($productPrice);
            $productFactory->setWeight($productPrice);
            // $productFactory->setData('weight', $Weight);
            $productFactory->save();

            $this->_categoryLinkManagement->assignProductToCategories(
                $productSku,
                $productCategoryIds
            );
        }

        print("Import completed\n");
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("hrm:zhifanimportcsvcommand:importcsv");
        $this->setDescription("Command to import CSV");
        $this->setDefinition([
            new InputArgument(self::PATH_CSV, InputArgument::REQUIRED, "Path to CSV"),
            // new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }

    private function slugify($string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }
}
