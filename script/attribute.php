<?php

// size
$attribute_id = $this->attributeRepository->get('catalog_product', 'size')->getAttributeId();
$options = $this->attributeOptionManagement->getItems('catalog_product', $attribute_id);

$output->writeln('productSKU Simple attribute_id Size = <comment>' . $attribute_id . '</comment>');
foreach ($options as $option) {
    // $output->writeln('Attribute Size = <comment>' . $option->getLabel() . '</comment>');
    $label_array[] = $option->getLabel();
    $value_array[] = $option->getValue();
}

print_r($label_array);
print_r($value_array);
$count_value = count($value_array);
$newCount = $count_value + 1;

if (in_array($newSize, $label_array)) {
    $output->writeln('productSKU Simple Size = <comment>' . $newSize . '</comment>');
} else {
    $output->writeln('productSKU Simple New Size = <comment>' . $newSize . '</comment>');
    $this->logger->info('productSKU Simple New Size = : ' . $skuSimple . ' / Size : ' . $newSize);
    // $this->option->setValue($newValue);
    $this->attributeOptionLabel->setStoreId(0);
    $this->attributeOptionLabel->setLabel($newSize);
    $this->option->setLabel($newSize);
    // $this->option->setStoreLabels([$this->attributeOptionLabel]);
    $this->option->setSortOrder($newCount);
    // $this->option->setIsDefault(false);
    $this->attributeOptionManagement->add('catalog_product', $attribute_id, $this->option);
}

 $size_id = $attributesize->getSource()->getOptionId($newSize); // get Xl option ID
 $products->setSize($size_id);


//stock
$products->setStockData(array(
    'use_config_manage_stock' => 0, //'Use config settings' checkbox
    'manage_stock' => 1, //manage stock
    'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
    'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
    'is_in_stock' => $isInStock, //Stock Availability 0 or 1
    'qty' => $sayQty //qty
        )
);

// category
$collection = $categoryFactory->create()->getCollection()->addAttributeToFilter('name', $name)->setPageSize(1);
if ($collection->getSize() || $name == NULL) {
    $categoryId = $collection->getFirstItem()->getId();
} else {
    $output->writeln('<error>Unable to read category</error>');
}
$products->setCategoryIds($categoryIds);
